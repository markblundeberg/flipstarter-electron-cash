#!/usr/bin/env python3
# Based on https://github.com/bboe/deterministic_zip/
# BSD 2-Clause "Simplified" License
# Copyright (c) 2019, Bryce Boe
# Copyright (c) 2019, AppFolio, Inc.
# Copyright (c) 2020, Flipstarter developers

import os
import stat
import sys
import zipfile

PLUGINNAME = "flipstarter"
DIRNAME = "flipstarter"

def check_version_str(version):
    import re
    if re.match(r"^\d+\.\d+$", version):
        return

    raise Exception(f"Invalid version string '{version}'.")

def read_version(manifest_path):
    import json
    with open(manifest_path) as fh:
        manifest = json.load(fh)
    version = manifest['version']
    check_version_str(version)
    return version

def sha256_checksum(filename, block_size=65536):
    import hashlib
    sha256 = hashlib.sha256()
    with open(filename, 'rb') as f:
        for block in iter(lambda: f.read(block_size), b''):
            sha256.update(block)
    return sha256.hexdigest()

def add_directory(zip_file, path, zip_path):
    for item in sorted(os.listdir(path)):
        current_path = os.path.join(path, item)

        if not current_path.endswith(".py"):
            print(f"Ignoring non-python file {current_path}")
            continue

        current_zip_path = os.path.join(zip_path, item)

        if not os.path.isfile(current_path):
            print(f"Ignoring directory {current_path}")
            continue

        add_file(zip_file, current_path, current_zip_path)


def add_file(zip_file, path, zip_path=None):
    permission = 0o555 if os.access(path, os.X_OK) else 0o444
    zip_info = zipfile.ZipInfo.from_file(path, zip_path)
    zip_info.date_time = (2020, 1, 1, 0, 0, 0)
    zip_info.external_attr = (stat.S_IFREG | permission) << 16
    with open(path, "rb") as fp:
        zip_file.writestr(
            zip_info,
            fp.read(),
            compress_type=zipfile.ZIP_DEFLATED,
            compresslevel=9,
        )


def main():
    if sys.version_info < (3, 7):
        sys.stderr.write("This script requires python 3.7+.\n")
        return 1

    scriptpath = os.path.dirname(os.path.abspath(__file__))
    manifest_path = os.path.normpath(
        os.path.join(scriptpath, "..", "manifest.json"))
    plugin_path = os.path.normpath(
        os.path.join(scriptpath, "..", DIRNAME))

    version = read_version(manifest_path)

    # Assumes this script is located in <root of repo>/contrib
    outpath = os.path.normpath(
        os.path.join(scriptpath, "..", f"{PLUGINNAME}-{version}.zip"))

    with zipfile.ZipFile(outpath, "w") as zip_file:

        try:
            add_directory(zip_file, plugin_path, os.path.basename(plugin_path))
            add_file(zip_file, manifest_path, "manifest.json")
        except Exception as e:
            import traceback
            sys.stderr.write(f"Error: {e}\n")
            sys.stderr.write("{}\n".format(traceback.format_exc()))
            os.unlink(outpath)
            return 0

    checksum = sha256_checksum(outpath)
    print(f"Wrote {outpath}\n -- checksum {checksum}")
    return 0

main()
