import unittest
from .known_addr import get_addr_owner, summarize_outputs, AUTO_EXPIRE

DUMMY = "bitcoincash:dummy1"
DUMMY2 = "bitcoincash:dummy2"
DUMMY3 = "bitcoincash:dummy2"
ADDED = 2000000000

class TestKnownAddr(unittest.TestCase):
    def test_get_addr_owner(self):
        self.assertIsNone(get_addr_owner("bitcoincash:unknownaddress"))
        self.assertIsNone(get_addr_owner(DUMMY, now = ADDED - 1))
        self.assertEqual("DUMMY 1", get_addr_owner(DUMMY, now = ADDED))
        self.assertEqual("DUMMY 1", get_addr_owner(DUMMY, now = ADDED + AUTO_EXPIRE))
        self.assertIsNone(get_addr_owner(DUMMY, now = ADDED + AUTO_EXPIRE + 1))

    def test_summarize_outputs(self):
        addr = [DUMMY, "bitcoincash:unknownaddress"]
        self.assertTrue(
                "has a destination unknown" in summarize_outputs(addr, now = ADDED))

        self.assertEqual(
                "This pledge appears to be for DUMMY 1.",
                summarize_outputs([DUMMY], now = ADDED))

        self.assertEqual(
                "This pledge appears to be for DUMMY 1 and DUMMY 2.",
                summarize_outputs([DUMMY, DUMMY2], now = ADDED))

        self.assertTrue(
            "appears to be to multiple" in summarize_outputs(
                [DUMMY, DUMMY2, DUMMY3], now = ADDED))
