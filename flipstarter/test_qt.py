from .qt import Plugin, Instance
from PyQt5.QtWidgets import QApplication
from unittest import mock
import os
import sys
import unittest

class TestPlugin(unittest.TestCase):

    def test_init(self):
        # Just see if we can at least  init the plugin with mock objects.

        parent = mock.MagicMock()
        config = mock.MagicMock()
        p = Plugin(parent, config, name = "unittest")

class TestInstance(unittest.TestCase):

    def test_diagnostic_name(self):
        if os.getenv('DISPLAY', None) is None:
            raise unittest.SkipTest("X server not available")

        app = QApplication(sys.argv)

        wallet = mock.MagicMock()
        i = Instance(
            plugin = mock.MagicMock(),
            wallet = wallet,
            window = mock.MagicMock())

        # Wallet does not have diagnostic_name method
        wallet.diagnostic_name = None
        self.assertEqual("Instance.flipstarter", i.diagnostic_name())

        # Wallet has diagnostic_name method
        wallet.diagnostic_name = lambda: "flipflop"
        self.assertEqual("flipflop.flipstarter", i.diagnostic_name())
